var crypto = require('crypto'),
	util = require('util'),
	request = require('request');

var secret = '<insert secret>',
	token = '<insert token>';

function generateAuthToken(method, path, timestamp) {
	var stringToSign = method + ' ' + path + ' ' + timestamp;

	var hmac = crypto.createHmac('SHA1', secret);
	hmac.update(stringToSign);

	return token + ':' + hmac.digest('hex');
}


function createOrderTest() {
	var timestamp = Date.now();

	var order = {
		destination: {
			name: 'progressive'
		},
		orderData: {
			sourceOrderId: timestamp,							// using timestamp for testing purposes to ensure unique ID
			postbackAddress: 'http://requestb.in/xotrnoxo',		// test postback url, visit to see the postback content
			postbackMethod: 'http',				
			email: 'customer@clientco.com',
			items: [
				{
					sourceItemId: timestamp,
					sku: 'apitest_8x8_hc',
					quantity: 1,
					components: [
						{
							code: 'text',
							fetch: true,
							path: 'https://s3-us-west-2.amazonaws.com/sampleprintfiles/sample-print-file.pdf'
						},
						{
							code: 'cover',
							fetch: true,
							path: 'https://s3-us-west-2.amazonaws.com/sampleprintfiles/sample-print-file.pdf'
						}
					]
				}
			],
			shipments: [
				{
					shipTo: {
						name: 'Peter Pan',
						companyName: 'Disney Corporation',
						address1: '1313 Disneyland Drive',
						town: 'Anaheim',
						state: 'CA',
						postcode: '92802',
						isoCountry: 'US'
					},
					carrier: {
						code: 'ups',
						service: 'ground'
					}
				}
			]
		}
	};

	request.post('https://stage.oneflowcloud.com/api/order', {
		json: true,
		body: order,
		headers: {
			'x-oneflow-authorization': generateAuthToken('POST', '/api/order', timestamp),
			'x-oneflow-date': timestamp
		}
	}, function(err, res, body) {
		console.log(util.inspect(body, false, null));
	});
}


createOrderTest();